# 自定义LoginRequiredMixin
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse


class LoginRequiredJSONMixin(LoginRequiredMixin):
    """
    实现重写登录扩展类里面handle用户未登录的方法
    如果用户未登录 响应json 且状态码为400
    """

    def handle_no_permission(self):
        return JsonResponse({'code': 400, 'errmsg': '用户未登录'})