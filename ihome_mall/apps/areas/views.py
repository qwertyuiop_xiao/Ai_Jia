from django.shortcuts import render
from django.views import View
# Create your views here.
from django_redis import cache
import json

from django.shortcuts import render
from django.views import View
from django.core.cache import cache
from django import http

from apps.homes.models import Area


class AreaListView(View):
    class AreaListView(View):
        """
        城区列表
        """

        def get(self, request):
            """
            城区列表业务逻辑实现
            :param request:
            :return:
            """
            # 获取缓存中的城区信息
            areas = cache.get('area_list')
            # 判断城区列表是否存在
            if areas:
                return http.JsonResponse({'errno': 0, 'errmsg': '获取成功', 'data': areas})

            try:
                area_mobile_list = Area.objects.all()
                print(area_mobile_list)
            except Area.DoesNotExist:
                return http.JsonResponse({'errno': 400, "errmsg": '数据库错误'})
            # 创建城区列表
            areas = []
            for area in area_mobile_list:
                areas.append({
                    "aid": area.id,
                    "aname": area.name
                })
            # 设置缓存
            cache.set('area_list', areas, 3600)
            # 返回响应
            return http.JsonResponse({'errno': 0, 'errmsg': '获取成功', 'data': areas})
class HouseView(View):
    pass
class SendHouseImageView(View):
    pass
class MyHouseRegisterView(View):
    pass
class HouseListView(View):
    pass

class HouseSearchView(View):
    pass
class HouseMessageView(View):
    pass