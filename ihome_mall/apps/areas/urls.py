from django.contrib import admin
from django.urls import path
from . import  views

urlpatterns = {
    #城区列表
    path('api/v1.0/areas', views.AreaListView.as_view()),
    #发布房源
    # path('api/v1.0/houses', views.HouseView.as_view()),
    #上传房源
    path('api/v1.0/houses/[int:house_id]/images', views.SendHouseImageView.as_view()),
    #我的房屋列表
    path('api/v1.0/user/houses', views.MyHouseRegisterView.as_view()),
    #首页推荐
    path('api/v1.0/houses/index', views.HouseListView.as_view()),
    #搜索
    path('api/v1.0/houses', views.HouseSearchView.as_view()),
    #详情
    path('api/v1.0/houses/[int:house_id]', views.HouseMessageView.as_view()),
}
