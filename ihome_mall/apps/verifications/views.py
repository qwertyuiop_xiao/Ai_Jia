from django.shortcuts import render
from django.views import View
# 使用HttpResponse返回图片数据
from django.http import HttpResponse, JsonResponse
# 使用django_redis模块连接配置文件中的redis数据库
from django_redis import get_redis_connection
from apps.verifications.libs.captcha.captcha import captcha
from apps.verifications.libs.yuntongxun.ccp_sms import CCP
# random随机生成验证码数字
import random
import json
import re

# Create your views here.
class ImageView(View):
    def get(self, request):
        # 1、接收参数
        cur_uuid = request.GET.get("cur")
        pre_uuid = request.GET.get("pre")
        # 1、生成图片验证码
        text, image = captcha.generate_captcha()
        # 2、保存图片验证码到redis中
        # 2-1、建立redis连接对象
        conn = get_redis_connection('verify')
        try:
            # 删除之前的
            conn.delete('image_%s' % pre_uuid)
            # 保存当前的
            conn.setex('image_%s' % cur_uuid, 300, text)
        except:
            return HttpResponse("生成图片验证码失败")
        else:
        # content_type指定返回的数据格式
            return HttpResponse(image, content_type='image/jpg')

class SMSCodeView(View):
    def post(self,request):
        # 1、接收参数
        dict_data = json.loads(request.body.decode())
        mobile = dict_data.get("mobile")
        imageCodeId = dict_data.get("id")
        imageCode = dict_data.get("text")
        # 补充：频繁发送短信的判断，主要判断两次请求之间的时间间隔是否在一分钟之内，如果在一分钟之内，说明请求过于频繁
        #  建立redis连接获取redis数据
        conn = get_redis_connection('verify')
        # 获取标志数据
        sms_code_flag = conn.get('sms_code_flag_%s' % mobile)
        # 如果获取到标志数据，说明数据未超过有效期，还在60s内，那么两次请求之间的间隔是在60s,请求过于频繁
        if sms_code_flag:
            return JsonResponse({'errno': 400, 'errmsg': '请求过于频繁'}, status=400)
        # 判断参数是否完整
        if not all([mobile,imageCode,imageCodeId]):
            return JsonResponse({'errno': '参数不完整'}, status=400)
        if not re.match(r"1[35678]\d{9}", mobile):
            return JsonResponse({"errno": 400, "errmsg": "手机输入有误"},status=400)

        # 2-2 从redis中获取验证码信息 注意：从redis中获取的数据是bytes类型
        real_smscode = conn.get('image_%s' % imageCodeId)
        real_smscode = real_smscode.decode()
        # 2-3 比对验证是否一致
        if real_smscode.lower() != imageCode.lower():
            return JsonResponse({'errno': '验证码错误'}, status=400)

        # 3、生成短信验证码
        # data=random.randint(0, 999999)
        # data1='%06d' % 123
        phonecode = '%06d' % random.randint(0, 999999)
        print('短信验证码是:%s'  % phonecode)
        # 4、保存短信验证码
        # conn.setex('sms_code_%s' % mobile, 300, sms_code)
        # # 增加判断时间间隔的标志数据
        # conn.setex('flag_%s' % mobile, 60, 'flag_data')
        # 使用管道写入数据
        # # 使用pipeline()生成管道对象
        pipe = conn.pipeline()
        # 将radis的写入数据的命令存入管道
        pipe.setex('sms_%s' % mobile, 300, phonecode)
        pipe.setex('flag_%s' % mobile, 60, 'flag_data')
        # 执行管道命令
        pipe.execute()
        # 5、发送短信
        ccp = CCP()
        ccp.send_template_sms(mobile, [phonecode, 5], 1)

        # 使用celery，异步调用发送短信方法,delay方法中传递参数
        # send_smscode.delay(mobile, sms_code)
        # 6、返回结果
        return JsonResponse({'errno':'ok','errmsg': '发送短信成功'})

