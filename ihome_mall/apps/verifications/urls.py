from django.contrib import admin
from django.urls import path
from . import views

urlpatterns={
    #图片验证
    path('imagecode/',views.ImageView.as_view()),
    #短信验证
    path('sms',views.SMSCodeView.as_view()),


}