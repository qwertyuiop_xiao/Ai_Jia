from django.contrib import admin
from django.urls import path
from . import  views

urlpatterns = {
    # 添加订单
    path('orders', views.Orders.as_view()),
    # 获取订单
    path('user/orders', views.Orders.as_view()),
    # 接单和拒单
    path('orders/<int:order_id>/status', views.Orders.as_view()),
    # 评价订单
    path('orders/<int:order_id>/comment', views.OrderComment.as_view()),
}