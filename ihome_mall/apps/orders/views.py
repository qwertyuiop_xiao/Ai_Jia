from django.shortcuts import render
from django.views import View
import json

from datetime import datetime
from apps.homes.models import House,HouseImage
from django.db import transaction
from ihome_mall.utils.views_zun import  LoginRequiredJSONMixin
from django.http import JsonResponse
from apps.orders.models import Order
from django_redis import get_redis_connection
# Create your views here.
#调用自定义登录，注意就近原则

class Orders(LoginRequiredJSONMixin,View):
    '''订单一系列功能'''
    '''获取订单'''
    def get(self,request):
        #获取用户参数
        user=request.user
        #获取角色类型参数
        role=request.GET.get('role')
        #校验参数
        #未获取到信息
        if not role:
            return JsonResponse({'code':400,'errmsg':'缺少参数'})
        #传参错误
        if role not in ['landlord','custom']:
            return JsonResponse({'code':400,'errmsg':'参数错误'})
        #判断是客户还是房东
        #客户功能
        if role=='custom':
        #客户查询自己所下的订单,并按照时间排序

            order_list=Order.objects.filter(user=user).order_by('create_time')
            orders=[]
            for order in order_list:
                orders.append({
                    "amount": order.amount,
                    "comment": order.comment,
                    "ctime": order.create_time.strftime("%Y-%m-%d"),
                    "days": order.days,
                    "end_date": order.end_date,
                    "img_url": order.house.index_image_url,
                    "order_id": order.id,
                    "start_date": order.begin_date,
                    "status": Order.ORDER_STATUS_ENUM[order.status],
                    "title": order.house.title
                })

        else:
            houses_list=Order.objects.filter(house__user=user).order_by('create_time')
            orders=[]
            for order in houses_list:
                orders.append({
                    "amount": order.amount,
                    "comment": order.comment,
                    "ctime": order.create_time.strftime("%Y-%m-%d"),
                    "days": order.days,
                    "end_date": order.end_date,
                    "img_url": order.house.index_image_url,
                    "order_id": order.id,
                    "start_date": order.begin_date,
                    "status": Order.ORDER_STATUS_ENUM[order.status],
                    "title": order.house.title
                })
        return JsonResponse({'code':0,'errmsg':'发布成功','data':{'orders':orders}})
    '''添加订单'''
    def post(self,request):
        user=request.user
        dict=json.loads(request.body.decode())
        house_id=dict.get('house_id')#房屋id
        start_date=dict.get('start_date')#开始日期
        end_date=dict.get('end_date')#结束时间
        #检验参数
        if not all([house_id,start_date,end_date]):
            return JsonResponse({'code':400,'errmsg':'参数不全'})
        if start_date>end_date:
            return JsonResponse({'code':400,'errmsg':'时间逻辑错误'})
        try:
            house=House.objects.get(id=house_id)
        except:
            return JsonResponse({'code':400,'errmsg':'数据库信息错误'})

        #对订单时间进行校验
        st = datetime.strptime(start_date, '%Y-%m-%d')
        en = datetime.strptime(end_date, '%Y-%m-%d')
        day_time=(en-st).days
        if house.max_days != 0:
            if house.max_days < day_time or house.min_days > day_time:
                return JsonResponse({"code": 400, 'errmsg': '请求天数不在范围内'})
        else:
            if house.min_days > day_time:
                return JsonResponse({"code": 400, 'errmsg': '请求天数不在范围内'})
        #下单信息校验
        if user.id==house.user.id:
            return JsonResponse({'code':400,'errmsg':'不能自己下单'})

        count=Order.objects.filter(house_id=house_id,
                                    begin_date__lte=end_date,
                                    end_date__gte=start_date).count()
        if count>0:
            return JsonResponse({"code":400,'errmsg':'房子已被预定过了'})
        amount=day_time*house.price
        order=Order.objects.create(
            user=request.user,
            house_id=house_id,
            begin_date=st,
            end_date=en,
            days=day_time,
            house_price=house.price,
            amount=amount,
            status=Order.ORDER_STATUS['WAIT_ACCEPT'],

        )
        return JsonResponse({"code": 0,
                             'errmsg': '下单成功',
                             'data': {
                                 "order_id": order.id
                             }})
    '''接单拒单功能'''

    def put(self, request, order_id):
        json_dict = json.loads(request.body.decode())
        action = json_dict.get('action')
        reason = json_dict.get('reason')
        if not all([action,order_id]):
            return JsonResponse({'code':400,'errmsg':'参数不齐'})
        if action=='reject':
            try:
                order=Order.objects.filter(id=order_id)
                order.update(comment=reason,status=Order.ORDER_STATUS['REJECTED'])
                house=House.objects.filter(id=order.house_id)
            except:
                JsonResponse({'code':400,'errmsg':'数据查询失败'})
            try:
                #拒单,房屋count订单状态从1改为0
                house.order_count -=1
                house.save()
            except:
                JsonResponse({'code':400,'errmsg':'更新房屋订单状态失败'})
        elif action=='accept':
            try:
                order=Order.objects.filter(id=order_id)
                order.update(status=Order.ORDER_STATUS['WAIT_COMMENT'])
            except:
                return JsonResponse({'code':400,'errmsg':'操作数据库失败'})

        else:
            return JsonResponse({"code":400,'errmsg':'参数错误'})
        return JsonResponse({'code':0,'errmsg':'操作成功'})
class OrderComment(LoginRequiredJSONMixin,View):
    def get(self,request,order_id):
        user=request.user
        json_dict=json.loads(request.body.decode())
        comment=json_dict.get('comment')
        #信息校验
        if not comment:
            return JsonResponse({'code':400,'errmsg':'评论为空'})
        #开启事务
        with transaction.atomic():
        #事务保护点
            save_id=transaction.savepoint()
            try:
                try:
                    order = Order.objects.filter(id=order_id, status=Order.ORDER_STATUS["WAIT_COMMENT"]).first()
                    house = order.house
                except:
                    return JsonResponse({'code':400,'errmsg':'查询失败'})
            #更新数据
                house.order_count+=1
                order.status=Order.ORDER_STATUS_ENUM['COMPLETE']
                order.comment=comment
                try:
                    house.save()
                    order.save()
                except:

                    return JsonResponse({'code':400,'errmsg':'数据库更新失败'})

            except:
                #回滚至保存点
                transaction.savepoint_rollback(save_id)
                return JsonResponse({'errno': 400, 'errmsg': '评价失败'})
            #清除事务保存点
            transaction.savepoint_commit(save_id)
        return JsonResponse({'code':0,'errmsg':'发布成功'})