from haystack import indexes

from .models import House


#文件名字是固定的
class HomeIndex(indexes.SearchIndex, indexes.Indexable):
    """SKU索引数据模型类"""
    text = indexes.CharField(document=True, use_template=True)

    def get_model(self):
        """返回建立索引的模型类"""
        return House#1.对哪个模型类进行索引的建立，其他的都是固定不变的
                        # 2.对哪个模型类的字段进行索引的建立，在具体在 templates/search/indexes/goods/sku_text.txt 文件中定义
                        # {{ object.name }}#商品名称
                        # {{ object.caption }}#商品的简介

                        #goods /在哪个子应用下面创建的
                        #根据指定索引数据，执行创建索引的指令python manage.py rebuilt_index

    def index_queryset(self, using=None):
        """返回要建立索引的数据查询集"""
        return self.get_model().objects.filter(order_count=0)
