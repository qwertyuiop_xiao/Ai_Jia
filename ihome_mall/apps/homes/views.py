# -*- coding: utf-8 -*-
from __future__ import unicode_literals


import json

from django import http
from django.http import JsonResponse
from django.shortcuts import render
from django.http import JsonResponse
# Create your views here.
from django.views import View
from apps.homes.models import HouseImage, House, Facility, Area
from apps.homes.utils import LoginRequiredJsonMixin

from apps.homes.models import HouseImage,House
from ihome_mall.settings.dev import logger

from ihome_mall.utils.qiniu_storage import storage
from django.core.paginator import Paginator


class HomeView(LoginRequiredJsonMixin,View):
    """
    上传房源图片
    """

    def post(self,request,house_id):
        house_image=request.POST.get('house_image')
        url1=storage(house_image)
        print(type(url1),url1)
        try:
            house_image = HouseImage()
            house_image.url = url1
            house_image.house_id = int(house_id)
            house_image.save()
        except Exception as e:
            print(e)

        url2='http://qew4h3cmi.hn-bkt.clouddn.com'+url1
        print(url2)
        my_dict={
            "url": url2
        }
        return http.JsonResponse({"data": my_dict,
                                    "errno": "0",
                                    "errmsg": "图片上传成功"})

class HomeListView(LoginRequiredJsonMixin,View):
    """
    获取房屋列表数据
    """
    def get(self,request):

        my_list=[]
        my_dict={}
        user=request.user
        houses=House.objects.filter(user_id=user.id)
        for house in houses:
            my_dict['address'] = house.address#House，可以通过user_id获取House表格数据
            area_name=house.area.name
            my_dict['area_name'] = area_name#House外键Area
            my_dict['ctime'] = house.create_time#哪个表都有
            my_dict['house_id'] = house.id#House
            house_images=house.houseimage_set.all()
            for house_image in house_images:
                my_dict['img_url'] = house_image.url  # HouseImage可以通过House外键获得
            my_dict['order_count'] = house.order_count#House
            my_dict['price'] = house.price#House
            my_dict['title'] = house.title#House
            my_dict['user_avatar'] = house.index_image_url#House
            my_list.append(my_dict)
        return http.JsonResponse({
            'data':{"houses":my_list},
            "errmsg": "ok",
            "errno": "0"

        })

from haystack.views import SearchView

class MySearchView(SearchView):
    '''重写SearchView类'''#定义搜索服务，类视图需要继承haystack客户端的类视图SearchView
    def create_response(self):#重写客户端返回方法，按照json方式进行返回
        page = self.request.GET.get('page')
        # 获取搜索结果
        context = self.get_context()#获取搜索结果，
        data_list = []
        #context是字典
        #context['page'].object_list:获取查询后的商品数据
        for sku in context['page'].object_list:
            data_list.append({#往空列表中添加商品数据
                'id':sku.object.id,
                'name':sku.object.name,
                'price':sku.object.price,
                'default_image_url':sku.object.default_image.url,
                'searchkey':context.get('query'),
                'page_size':context['page'].paginator.num_pages,#获取总页数
                'count':context['page'].paginator.count#获取总页数
            })
        # 拼接参数, 返回
        return JsonResponse(data_list, safe=False)#因为返回的是列表，不需要进行字典类项检查，所以safe=False
class HomeSearchView(LoginRequiredJsonMixin,View):
    def get(self,request):
        aid=request.GET.get('aid')#区域id
        start_day=request.GET.get('sd')#开始日期
        end_day=request.GET.get('ed')#结束日期
        sort_key=request.GET.get('sk')#排序方式 booking(订单量), price-inc(低到高), price-des(高到低)
        page=request.GET.get('p',1)#页数，不传默认为1
        my_houses = []
        my_dict = {}
        user = request.user
        houses_ones = House.objects.filter(user_id=user.id)
        for house_one in houses_ones:

            try:#根据商品分类id获取商品分类对应的所有商品
                houses=House.objects.filter(area_id=aid,order_count=0).order_by(sort_key)
                paginator = Paginator(houses, 5)  # 先拿到分页器对象，第一个参数：对象列表，第二个参数：每页显示的条数

                # paginator.count  # 总条数
                count=paginator.num_pages  # 总页数

                current_page = paginator.page(page)  # 取某一页，返回一个对象
                print(current_page)
                for house in current_page:#遍历该对象，返回每一页中每一个商品数据
                    my_dict['address']=house.address#房屋地址
                    area_name = house.area.name
                    my_dict['area_name']=area_name#城区名
                    my_dict['ctime']=house.create_time#创建时间
                    my_dict['house_id']=house.id#房屋id
                    house_images = house.houseimage_set.all()
                    for house_image in house_images:
                        my_dict['img_url'] = house_image.url  # HouseImage可以通过House外键获得
                    my_dict['order_count']= house.order_count#订单数据
                    my_dict['price']=house.price#价格
                    my_dict['room_count']='room_count'#房间数目
                    my_dict['title']=house.title#标题
                    my_dict['user_avatar']=house.index_image_url#该房屋所有者的头像
                    my_houses.append(my_dict)
            except Exception as e:
                print(e)
                return http.JsonResponse({'code':0,'errmsg':'查询不到数据'})
        return http.JsonResponse({'data':{'houses':my_houses,'total_page': count},
                                  'errmsg':'请求成功',
                                  'errno':0
                                  })


class HomeReferView(View):

    def get(self,request):
        # 在数据库中获取数据
        # houses = HouseImage.objects.all().filter(house_id__in=(5,6,7,8,9))
        # house_list = []
        # for house in houses:
        #     house_dict = {}
        #     house_dict['house_id']=house.house,
        #     house_dict['img_url']=house.url,
        #     house_dict['title']=house.house.title,
        #     house_list.append(house_dict)
        # return JsonResponse({
        #                     "data": house_list,
        #                     "errmsg": "OK",
        #                     "errno": "0"
        #                     })
        try:
            (house1,house2,house3,house4,house5) = HouseImage.objects.all().filter(house_id__in=(5,6,7,8,9))
        except:
            return JsonResponse({"errmsg": "数据库连接失败","errno": "0"})
        response = JsonResponse({
            "data": [
                {
                    "house_id": house1.house.id,
                    "img_url": house1.url,
                    "title": house1.house.title
                },
                {
                    "house_id": house2.house.id,
                    "img_url": house2.url,
                    "title": house2.house.title
                },
                {
                    "house_id": house3.house.id,
                    "img_url": house3.url,
                    "title": house3.house.title
                },
                {
                    "house_id": house4.house.id,
                    "img_url": house4.url,
                    "title": house4.house.title
                },
                {
                    "house_id": house5.house.id,
                    "img_url": house5.url,
                    "title": house5.house.title
                },
            ],
            "errmsg": "OK",
            "errno": "0"

        })
        return response


class HouseView(View):
    """
    发布房源
    """
    def post(self, request):
        """
        发布房源业务逻辑实现
        :param request:
        :return:
        """
        # 获取当前用户
        user = request.user
        # 测试
        user.id=1
        # 获取前端传递的数据信息
        dict_json = json.loads(request.body.decode())

        title = dict_json.get('title')
        price = dict_json.get('price')
        area_id = dict_json.get('area_id')
        address = dict_json.get('address')
        room_count = dict_json.get('room_count')
        acreage = dict_json.get('acreage')
        unit = dict_json.get('unit')
        capacity = dict_json.get('capacity')
        beds = dict_json.get('beds')
        deposit = dict_json.get('deposit')
        min_days = dict_json.get('min_days')
        max_days = dict_json.get('max_days')

        # 校验参数
        # 1. 判断参数是否完整
        if not all([title, price, area_id, address, room_count, acreage,
                    unit, capacity, beds, deposit, min_days, max_days]):
            return http.JsonResponse({'errno': 400, 'errmsg': '缺少参数'})

        # 2. 对参数格式进行判断
        # (1) 价格判断
        if int(price) < 0:
            return http.JsonResponse({'errno': 400, 'errmsg': '价格不正确'})
        # (2) 城区id判断, 城区是否在数据库中存在
        try:
            area = Area.objects.get(id=area_id)
        except:
            return http.JsonResponse({'errno': 400, 'errmsg': 'area_id不存在'})
        # (3) 房间数据判断
        if int(room_count) < 0:
            return http.JsonResponse({'errno': 400, 'errmsg': '房间数不正确'})

        # (4) 房屋面积判断
        if int(acreage) < 0:
            return http.JsonResponse({'errno': 400, 'errmsg': '房屋面积不正确'})
        # (5) 房屋容纳人数判断
        if int(capacity) < 0:
            return http.JsonResponse({'errno': 400, 'errmsg': '房屋容纳人数不正确'})
            # (6) 押金判断
        if int(deposit) < 0:
            return http.JsonResponse({'errno': 400, 'errmsg': '押金不正确'})
        # (7) 入住天数判断
        if int(max_days) < 0 or int(min_days) < 0:
            return http.JsonResponse({'errno': 400, 'errmsg': '入住天数不正确'})
        # (8) 最小天数和最大天数对比
        if int(max_days) != 0:
            if int(min_days) > int(max_days):
                return http.JsonResponse({'errno': 400, 'errmsg': '入住天数不正确'})

        try:
            area = Area.objects.get(id=area_id)
        except Exception as e:
            logger.error(e)
            return http.JsonResponse({"errno": 400, "errmsg": "地址不存在"})

        # 保存房源信息
        home_info = House.objects.create(
            title=title,
            price=price,
            area_id=area_id,
            address=address,
            room_count=room_count,
            acreage=acreage,
            unit=unit,
            capacity=capacity,
            beds=beds,
            deposit=deposit,
            min_days=min_days,
            max_days=max_days,
            user_id=user.id,
        )


        # 保存设施信息
        # 1. 获取前端传递的设施信息
        facility_ids = dict_json.get('facility')
        # 2. 根据id查询设施信息
        if facility_ids:
            facility_list = Facility.objects.filter(id__in=facility_ids)
            for facility in facility_list:
                home_info.facility.add(facility)


        # 返回结果
        return http.JsonResponse({
            'errno': 0,
            'errmsg': '发布成功',
            'data': {
                'house_id': home_info.id
            }})


from django.contrib import admin
from django.urls import path
from . import views


urlpatterns = [
    #发布房源
    path('api/v1.0/houses', views.HouseView.as_view()),
    # path('houses/<int:house_id>/images/',views.HomeView.as_view()),
    # path('user/houses/',views.HomeListView.as_view()),
]
