from django.contrib import admin
from django.urls import path
from . import  views

urlpatterns = {
    #发布房源
    path('api/v1.0/houses', views.HouseView.as_view()),
    path('houses/<int:house_id>/images/',views.HomeView.as_view()),
    path('user/houses/',views.HomeListView.as_view()),
    # 首页房屋推荐
    path('houses/index/', views.HomeReferView.as_view()),
}
