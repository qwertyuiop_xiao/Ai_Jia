from django import http
from django.contrib.auth.mixins import LoginRequiredMixin


class LoginRequiredJsonMixin(LoginRequiredMixin):
    #因为LoginRequiredMixin中的handle_no_permission采用后端逻辑进行的跳转，所以要重写handle_no_permission方法
    def handle_no_permission(self):#重写父类LoginRequiredMixin中的handle_no_permission方法，优先使用子类重写的方法
        return http.JsonResponse({
                'code': 400,
                'errmsg': '用户未登录'
            })
