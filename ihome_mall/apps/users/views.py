from django.shortcuts import render
from django.views import View
from django.http import JsonResponse
# 导入json模块
import json
# 导入用户模型类进行查询
from apps.users.models import User
# 导入re模块
import re
# 导入连接redis的方法
from django_redis import get_redis_connection
# 导入login方法进行状态保持
from django.contrib.auth import login,logout
from django.http import HttpRequest

# Create your views here.
class RegisterView(View):
    # 用户注册
    def post(self, request):
        # 1、接受前端传递的json数据 是bytes类型数据
        json_data = request.body.decode()
        # 2、将json转化字典数据
        # 3、从字典中提取相关的字段数据
        dict_data = json.loads(json_data)
        # 3、从字典中提取相关的字段数据
        mobile = dict_data.get('mobile')
        phonecode = dict_data.get("phonecode")
        password = dict_data.get('password')
        password2 = dict_data.get('password2')
        # 4、验证字段数据
        # 4-1、验证数据完整性all方法中如果有空值会返回false
        if not all([mobile, phonecode,password,password2]):
            return JsonResponse({'errno': 400, 'errmsg': '参数不完整'})
        # 4-3、判断手机号格式
        if not re.match(r'^1[3-9]\d{9}$', mobile):
            return JsonResponse({'errno': 400, 'errmsg': '手机号格式不正确'}, status=400)
        # 4-4、判断密码格式
        if not re.match(r'^[a-zA-Z0-9]{8,20}$', password):
            return JsonResponse({'errno': 400, 'errmsg': '密码格式不正确'}, status=400)
        # 4-5、判断两次是否一致
        if password != password2:
            return JsonResponse({'errno': 400, 'errmsg': '密码不一致'}, status=400)

            # 4-7、短信验证判断
            # 连接redis
        conn = get_redis_connection('verify')
        # 获取reids中存储的短信验证码
        real_code = conn.get('phonecode_%s' % mobile)
        # 判断real_code是否超过有效期
        if real_code is None:
            return JsonResponse({'errno': 400, 'errmsg': '验证码失效'}, status=400)
        # 判断用户输入的短信验证码和redis中的是否一致
        if  phonecode != real_code.decode():
            return JsonResponse({'errno': 400, 'errmsg': '验证码输入错误'}, status=400)
        # 5、将数据保存在数据库中
        #     create()保存数据是不会对用户密码加密create_user() 会对用户密码进行加密
        # user = User.objects.create(username=username,password=password,mobile=mobile)
        user = User.objects.create_user( username=mobile,password=password, mobile=mobile)
        # 用户注册成功，需要进行状态保持,第一个参数request对象,第二个参数是用户对象
        login(request, user)
            # 6、返回结果
        return JsonResponse({'errno':0,'errmsg': '注册成功'})

class LoginView(View):
    # 登录
    def post(self, request):
        # 1、获取前端传递的用户数据,是json格式,类型是bytes
        json_data = request.body.decode()
        # 2、将json数据转化为字典数据
        dict_data = json.loads(json_data)
        # 3、从字典中提取用户名字段数据，密码字段数据，记住密码状态数据
        mobile = dict_data.get('mobile')
        password = dict_data.get('password')
        # 4、验证数据
        # 4-1 验证数据完整性
        if not all([mobile, password]):
            return JsonResponse({'errno': 400, 'errmsg': '参数缺失'}, status=400)
        # 4-2 验证用户名是否存在
        try:
            user = User.objects.get(username=mobile)
        except:
            return JsonResponse({'errno': 400, 'errmsg': '用户不存在'}, status=400)
        # 4-3 验证密码
        if not user.check_password(password):
            return JsonResponse({'errno': 400, 'errmsg': '密码不正确'}, status=400)
        # 5、数据验证没有问题，用户存在则登录成功进行状态保持
        login(request, user)
        # 6、返回结果，返回时根据记住密码状态数据
        response = JsonResponse({'errno': 0, 'errmsg': '登录成功'})

        return response

    # 判断是否登录
    def get(self, request):
        if not request.user.is_authenticated:
            return JsonResponse({"errno": "4101", "errmsg": "未登录"})

        data = {
            'name': request.user.username,
            'user_id': request.user.id,
        }

        return JsonResponse({"errno": "0", "errmsg": "已登录", "data": data})

    # 退出登录
    def delete(self, request):
        # 清理登录状态logout()
        logout(request)
        # 清理用户名cookie
        response = JsonResponse({"errno": "0", "errmsg": "已登出"})
        response.delete_cookie('mobile')
        # 3、返会结果
        return response

class LogoutView(View):
    pass

class UserInfoView(View):
    pass
class SendImageView(View):
    pass

class ChangeNameView(View):
    pass
class CertificationView(View):
    pass