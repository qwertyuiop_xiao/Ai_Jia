from django.contrib import admin
from django.urls import path
from . import  views

urlpatterns = {
    # 用户注册
    path('users', views.RegisterView.as_view()),
    # 登录
    path('session', views.LoginView.as_view()),


}